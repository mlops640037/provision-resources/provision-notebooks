provider "google" {
  # Configuration options
  project     = var.project_id
  region      = var.region
  credentials = var.credentials
}

#Instantiating 
module "notebooks" {
  source            = "gitlab.com/mlops640037/51903553-google-notebook/google"
  version           = "0.0.0-e637a9d5"
  for_each          = var.steward
  name              = each.key
  project_id        = var.project_id
  service_account   = var.service_account
  location          = each.value.location
  machine_type      = each.value.vm_type
  boot_disk_type    = var.boot_disk_type
  boot_disk_size_gb = var.boot_disk_size_gb
  region            = var.region
  network           = var.network
  subnet            = var.subnet
  image_name        = var.image_name
  # you can  add image_name, 
}

module "cloudrun" {
  source             = "gitlab.com/mlops640037/53733880-google-cloud-run/google"
  version            = "0.0.0-74937cd2"
  project            = var.project_id
  name               = var.name
  location           = var.region
  ingress            = var.ingress
  use_default        = var.use_default
  min_instance_count = var.min_instance_count #1
  max_instance_count = var.max_instance_count #3
  service_account    = var.service_account
  image              = var.image
  cpu                = var.cpu
  memory             = var.memory
  container_port     = var.container_port
}

module "bucket" {
  source                      = "gitlab.com/mlops640037/54303700-google-bucket/google"
  version                     = "0.0.0-06d7fc20"
  project                     = var.project_id
  for_each                    = toset(var.bucket_name_set)
  name                        = each.value
  location                    = var.region
  force_destroy               = var.force_destroy
  storage_class               = var.storage_class
  uniform_bucket_level_access = var.uniform_bucket_level_access
  autoclass_bool              = var.autoclass_bool
  #terminal_storage_class      = var.terminal_storage_class
  versioning_bool          = var.versioning_bool
  public_access_prevention = var.public_access_prevention
}
