steward = {
  "notebook-1" = { vm_type = "n1-standard-1", location = "us-central1-b" }
  "notebook-2" = { vm_type = "e2-highmem-2", location = "us-central1-a" }
}

project_id      = "mlops-dev-406321"
region          = "us-central1"
service_account = "gitlab-wif-demo@mlops-dev-406321.iam.gserviceaccount.com"
boot_disk_type  = "PD_SSD"
network         = "default"
subnet          = "default"
image_name      = "common-cpu-notebooks-v20221107"

#cloudrun
name               = "test-service"
use_default        = false
min_instance_count = 1
max_instance_count = 3
image              = "gcr.io/mlops-dev-406321/test_api_image@sha256:17ccbadb00cd1d75c3a0a04c937dacd0b607add0ec95c63b7e392c27101b2254"
cpu                = "2"
memory             = "1024Mi"
container_port     = 8000


#bucket 
bucket_name_set = [
  "mlops-dev-data-bucket-001",
  "mlops-dev-data-bucket-002",
]
force_destroy               = true
storage_class               = "STANDARD"
uniform_bucket_level_access = true
autoclass_bool              = true
terminal_storage_class      = "NEARLINE"
versioning_bool             = false
public_access_prevention    = "enforced"



