variable "project_id" {
  type        = string
  description = "google cloud project ID"
  default     = null
}

variable "region" {
  type        = string
  description = "region where you want notebook to be created"

}

variable "credentials" {
  type        = string
  description = "service account keys"
  default     = null
}

variable "service_account" {
  type        = string
  description = "service account used to create resources"
  default     = null
}

variable "boot_disk_type" {
  type        = string
  description = "value"
  default     = "PD_SSD"

}

variable "boot_disk_size_gb" {
  type        = string
  description = "value"
  default     = 100
}


variable "network" {
  type        = string
  description = "value"
  default     = "default"
}


variable "subnet" {
  type        = string
  description = "name of the subnetwork the notebook instance will be on"
  default     = "default"
}


variable "image_name" {
  type        = string
  description = "VM image name"
  default     = "common-cpu-notebooks-v20221107"
}

variable "steward" {
  type = map(object({
    vm_type  = string
    location = string

  }))

  default = {
    "steward-nb1" = { vm_type = "n1-standard-1", location = "us-central1-b" }
    "steward-nb2" = { vm_type = "e2-highmem-2", location = "us-central1-a" }
  }
}

#####cloud run variables 

variable "name" {
  type        = string
  description = "name of the cloud run service"
}

variable "ingress" {
  description = "controls the type of inbound traffic cloud run service can allow to access container"
  type        = string
  default     = "INGRESS_TRAFFIC_INTERNAL_ONLY"
}

variable "use_default" {
  description = "disable/enable binary authorization"
  type        = bool
}

variable "min_instance_count" {
  description = "minimum number of container instances"
  type        = number
}

variable "max_instance_count" {
  description = "maximum number of container instances"
  type        = number
}

variable "image" {
  description = "container image used in cloud run service"
  type        = string

}

variable "cpu" {
  description = "the cpu limit per container"
  type        = string
}

variable "memory" {
  description = "the memory limit per container"
  type        = string
}

variable "container_port" {
  description = "the exposed port of the container"
  type        = number
}

####### bucket variables 
variable "bucket_name_set" {
  description = "A set of GCS bucket names..."
  type        = list(string)
}

variable "force_destroy" {
  description = "if true,you do not need to delete objects in bucket to delete bucket"
  type        = bool
}

variable "storage_class" {
  description = "the default storage class of object stored"
  type        = string

}

variable "uniform_bucket_level_access" {
  description = "if true, all objects will have the same access level in the bucket"
  type        = string
}

variable "autoclass_bool" {
  description = "allow google to control the storage class of objects stored in bucket"
  type        = string
}

# variable "terminal_storage_class" {
#   description = "the new storage class of an object after it has not been accessed for a period of time"
#   type        = string
# }

variable "versioning_bool" {
  description = "if false, only the latest version of your object is tracked"
  type        = string
}

variable "public_access_prevention" {
  description = "controls who can access the bucket publicly"
  type        = string
}